FROM python:3.8.5-alpine

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update && \
    # postgresql dependecies
    apk add --no-cache postgresql-dev gcc python3-dev musl-dev && \
    # ldap dependecies
    apk add --no-cache openldap-dev

RUN pip install --upgrade pip
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY . /app/
