from django.conf import settings
from django.contrib import admin
from django.urls import include, path


phonebook_patterns = [
    path('admin/', admin.site.urls),
    path('users/', include('django.contrib.auth.urls')),
    path('', include('book.urls')),
]

# creating a specifix prefix for the website
urlpatterns = [
    path(settings.PREFIX_URL, include(phonebook_patterns)),
]
