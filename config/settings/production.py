from .base import *


SECRET_KEY = os.environ.get('SECRET_KEY')

DEBUG = False

ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")


#LDAP
AUTH_LDAP_SERVER_URI = os.environ.get("AUTH_LDAP_SERVER_URI")

AUTH_LDAP_BIND_DN = os.environ.get("AUTH_LDAP_BIND_DN")
AUTH_LDAP_BIND_PASSWORD = os.environ.get("AUTH_LDAP_BIND_PASSWORD")

AUTH_LDAP_USER_SEARCH = LDAPSearch(
            "dc=sukl,dc=cz",
            ldap.SCOPE_SUBTREE,
            "(sAMAccountName=%(user)s)"
)

AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
            "dc=sukl,dc=cz",
            ldap.SCOPE_SUBTREE,
            "(objectClass=group)"
)

AUTH_LDAP_GROUP_TYPE = ActiveDirectoryGroupType(name_attr="cn")

AUTH_LDAP_USER_ATTR_MAP = {
            "username": "sAMAccountName",
            "first_name": "givenName",
            "last_name": "sn",
            "email": "mail",
}

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
            "is_active": ["cn=django_admins,ou=sukl_groups,dc=sukl,dc=cz",
                          "cn=django_evidence_rozboru_users,ou=sukl_groups,dc=sukl,dc=cz"],
            "is_superuser": "cn=django_admins,ou=sukl_groups,dc=sukl,dc=cz",
            "is_staff": "cn=django_admins,ou=sukl_groups,dc=sukl,dc=cz",
}

AUTH_LDAP_FIND_GROUP_PERMS = True

AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600

AUTHENTICATION_BACKENDS = [
            'django_auth_ldap.backend.LDAPBackend',
            'django.contrib.auth.backends.ModelBackend',
]

AUTH_LDAP_CONNECTION_OPTIONS = {
            ldap.OPT_REFERRALS: False,
}
