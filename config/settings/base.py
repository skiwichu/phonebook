from copy import deepcopy
import os
from pathlib import Path

import dj_database_url
from django_auth_ldap.config import ActiveDirectoryGroupType, LDAPSearch
from django.utils.log import DEFAULT_LOGGING
import graypy
import ldap


BASE_DIR = Path(__file__).resolve().parent.parent.parent


# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    # third parties
    'django_tables2',
    'django_filters',
    'crispy_forms',
    'bootstrap4',
    # local
    'users',
    'book',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

PREFIX_URL = 'phonebook/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(BASE_DIR.joinpath('templates'))],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
db_from_env = dj_database_url.config(conn_max_age=500)

DATABASES = {
    'default': db_from_env
}


# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
LANGUAGE_CODE = 'cs'
# LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Prague'
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATE_INPUT_FORMATS = ["%d.%m.%Y"]


# Static files (CSS, JavaScript, Images)
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]  # TODO
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')  # TODO
STATIC_URL = '/' + PREFIX_URL + 'staticfiles/'


# Crispy forms
CRISPY_TEMPLATE_PACK = 'bootstrap4'


# Logging
logging_dict = deepcopy(DEFAULT_LOGGING)
logging_dict['handlers'].update(
    {
        'graylog': {
            'level': 'WARNING',
            'class': 'graypy.GELFUDPHandler',
            'host': os.environ.get('GRAYLOG_HOST'),
            'port': int(os.environ.get('GRAYLOG_PORT')),
            'localname': 'phonebook',
        }
    }
)
logging_dict['loggers']['django']['handlers'] = []
logging_dict['loggers'].update(
    {
        '': {
            'handlers': ['graylog', 'console'],
        }
    }
)
LOGGING = logging_dict


# Custom User
AUTH_USER_MODEL = 'users.CustomUser'
#LOGIN_REDIRECT_URL = 'drug_list'  # TODO!
#LOGOUT_REDIRECT_URL = 'drug_list'  # TODO!
