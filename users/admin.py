from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('username', 'first_name', 'last_name', 'email',)
    fieldsets = (
        (None, {'fields':
            ('username', 'password', 'first_name', 'last_name', 'email', 'is_staff',)}
        ),
    )
    add_fieldsets = (
        (None, {'fields':
            ('username', 'password1', 'password2', 'first_name', 'last_name', 'email', 'is_staff',)}
        ),
    )

admin.site.register(CustomUser, CustomUserAdmin)
