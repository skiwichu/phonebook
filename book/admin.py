from django.contrib import admin

from .models import Department, Phonebook


admin.site.register(Department)
admin.site.register(Phonebook)
