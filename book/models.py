from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.db import models


PHONE_CODE = '282 185'

BUILDINGS = (
    ('8', 'budova 8'),
    ('10', 'budova 10'),
    ('24', 'budova 24'),
    ('30', 'budova 30'),
    ('Benešovská', 'Benešovská'),
    ('Brno', 'Brno'),
    ('České Budějovice', 'České Budějovice'),
    ('Hradec Králové', 'Hradec Králové'),
    ('Olomouc', 'Olomouc'),
    ('Ostrava', 'Ostrava'),
    ('Plzeň', 'Plzeň'),
)


class Phonebook(models.Model):
    title = models.CharField(max_length=15, blank=True, default='')
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=35)
    personal_number = models.PositiveIntegerField(
        unique=True,
        validators=[MinValueValidator(10000), MaxValueValidator(99999)],
    )
    email = models.EmailField(unique=True, default='@sukl.cz')
    landline_phone = models.CharField(
        max_length=9,
        default=PHONE_CODE.replace(" ", ""),
        unique=True,
        validators=[RegexValidator(
            r'^' + f'{PHONE_CODE.replace(" ", "")}' + r'[0-9]{3}$',
            f'Číslo musí být ve formátu {PHONE_CODE} 123 (tj. bez +420), bez mezer a s předvolbou {PHONE_CODE}.',
            )],
    )
    cell_phone = models.CharField(
        max_length=9,
        blank=True,
        null=True,
        unique=True,
        validators=[RegexValidator(
            r'^$|[0-9]{9}$',
            'Číslo musí být ve formátu 606 123 456 (tj. bez +420) a bez mezer.',
            )],
    )
    department = models.ForeignKey('Department', on_delete=models.SET_NULL, null=True)
    building = models.CharField(max_length=20, choices=BUILDINGS, blank=True, default='24')  # add contraint. if no room then no building
    room = models.CharField(max_length=5, blank=True, default='')

    def __str__(self):
        return f'{self.first_name[0]}. {self.last_name}'


class Department(models.Model):
    name = models.CharField(max_length=40, unique=True, null=False)
    abbreviation = models.CharField(max_length=5, unique=True, null=False)
    code = models.PositiveIntegerField(
        unique=True,
        validators=[MinValueValidator(10000), MaxValueValidator(99999)],
    )
    boss = models.ForeignKey(
        'Phonebook',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='boss',
    )
    deputy = models.ForeignKey(
        'Phonebook',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='deputy',
    )

    def __str__(self):
        return f'{self.abbreviation}'