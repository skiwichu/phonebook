import django_tables2 as tables

from .models import Phonebook


class PhonebookTable(tables.Table):
    name = tables.Column(empty_values=(), verbose_name='Jméno')
    personal_number = tables.Column(verbose_name='Osobní číslo')
    email = tables.Column(verbose_name='Email')
    department = tables.Column(verbose_name='Oddělení')
    building = tables.Column(verbose_name='Budova')
    room = tables.Column(verbose_name='Místnost')
    landline_phone = tables.Column(verbose_name='Telefonní linka')
    cell_phone = tables.Column(verbose_name='Mobilní telefon')

    class Meta:
        model = Phonebook
        exclude = {'id', 'title', 'first_name', 'last_name'}
        sequence = (
            'name',
            'personal_number',
            'email',
            'department',
            'building',
            'room',
            'landline_phone',
            'cell_phone',
        )
        show_header = True

    def render_name(self, value, record):
        if record.title:
            return f'{record.first_name} {record.last_name}, {record.title}'
        else:
            return f'{record.first_name} {record.last_name}'
    
    def render_phone(self, phone):
        return f'{phone[:3]} {phone[3:6]} {phone[6:9]}'

    def render_landline_phone(self, value):
        return self.render_phone(value)
    
    def render_cell_phone(self, value):
        return self.render_phone(value)