from django.urls import path

from .views import PhonebookView


urlpatterns = [
    path('', PhonebookView.as_view(), name='phonebook'),
]
