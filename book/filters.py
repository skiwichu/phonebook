from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Layout, Row, Submit
from django.db.models import Q
import django_filters 

from .models import Department, Phonebook


class PhonebookFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(method='name_filter', label='Jméno')
    personal_number = django_filters.NumberFilter(lookup_expr='contains', label='Osobní číslo')
    phone = django_filters.CharFilter(method='phone_filter', label='Telefon')
    department = django_filters.ModelChoiceFilter(queryset=Department.objects.all(), label='Oddělení')
    room = django_filters.CharFilter(lookup_expr='icontains', label='Mísnost')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form.helper = FormHelper()
        self.form.helper.form_method = 'get'
        self.form.helper.attrs = {"novalidate": "novalidate"}
        self.form.helper.add_input(Submit('submit', 'Vyhledat', css_class='btn-success'))
        self.form.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-8'),
                Column('personal_number', css_class='form-group col-md-4'),
            ),
            Row(
                Column('phone', css_class='form-group col-md-4'),
                Column('room', css_class='form-group col-md-4'),
                Column('department', css_class='form-group col-md-4'),
            ),
        )

    class Meta:
        model = Phonebook
        fields = []
    
    def name_filter(self, queryset, name, value):
        q = Q(title__icontains=value)
        q |= Q(first_name__unaccent__icontains=value)
        q |= Q(last_name__unaccent__icontains=value)
        return queryset.filter(q)

    def phone_filter(self, queryset, name, value):
        q = Q(landline_phone__contains=value)
        q |= Q(cell_phone__contains=value)
        return queryset.filter(q)
