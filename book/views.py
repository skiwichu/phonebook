from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

from .filters import PhonebookFilter
from .models import Phonebook
from .tables import PhonebookTable


class PhonebookView(SingleTableMixin, FilterView):
    table_class = PhonebookTable
    filterset_class = PhonebookFilter
    model = Phonebook
    template_name = 'phonebook.html'
    paginate_by = 2

    def get_table_data(self):
        qs = self.model.objects.all()
        filtered_list = PhonebookFilter(self.request.GET, queryset=qs)
        return filtered_list.qs