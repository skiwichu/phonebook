from http import HTTPStatus

from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.utils import IntegrityError
from django.test import TestCase

from .models import Department, Phonebook


class TestModelsDepartment(TestCase):
    def test_valid_data(self):
        department = Department.objects.create(
            name='test',
            abbreviation='TEST',
            code=10001,
        )
        department.full_clean()
        self.assertIn(department, Department.objects.all())
    
    def test_invalid_no_data(self):
        try:
            Department.objects.create()
        except IntegrityError as e:
            self.assertIn('code', repr(e))
        else:
            raise Exception

    def test_invalid_no_character_data(self):
        department = Department.objects.create(code=12345)
        try:
            department.full_clean()
        except ValidationError as e:
            self.assertIn('name', e.message_dict)
            self.assertIn('abbreviation', e.message_dict)
        else:
            raise Exception
    
    def test_invalid_code_small(self):
        department = Department.objects.create(
            name='test',
            abbreviation='TEST',
            code=1,
        )
        try:
            department.full_clean()
        except ValidationError as e:
            self.assertIn('code', e.message_dict)
        else:
            raise Exception
    
    def test_invalid_code_large(self):
        department = Department.objects.create(
            name='test',
            abbreviation='TEST',
            code=111111,
        )
        try:
            department.full_clean()
        except ValidationError as e:
            self.assertIn('code', e.message_dict)
        else:
            raise Exception


class TestModelsPhonebook(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        Department.objects.create(
            name='test',
            abbreviation='TEST',
            code=10001,
        )
        Phonebook.objects.create(
            first_name='Milos',
            last_name='Riha',
            personal_number=10000,
            email='milos@sukl.cz',
            landline_phone='282185123',
            department=Department.objects.first(),
            building='24',
            room='666',
        )

    def test_valid_data(self):
        book = Phonebook.objects.last()
        book.full_clean()
        self.assertIn(book, Phonebook.objects.all())
    
    def test_invalid_no_data(self):
        try:
            Phonebook.objects.create()
        except IntegrityError as e:
            self.assertIn('personal_number', repr(e))
        else:
            raise Exception

    def test_invalid_no_char_data(self):
        phonebook = Phonebook.objects.create(personal_number=12345)
        try:
            phonebook.full_clean()
        except ValidationError as e:
            self.assertIn('first_name', e.message_dict)
            self.assertIn('last_name', e.message_dict)
            self.assertIn('email', e.message_dict)
            self.assertIn('landline_phone', e.message_dict)
            self.assertIn('department', e.message_dict)
        else:
            raise Exception

    def test_invalid_personal_number_small(self):
        book = Phonebook.objects.last()
        book.personal_number = 1234
        try:
            book.full_clean()
        except ValidationError as e:
            self.assertIn('personal_number', e.message_dict)
        else:
            raise Exception

    def test_invalid_personal_number_large(self):
        book = Phonebook.objects.last()
        book.personal_number = 123456
        try:
            book.full_clean()
        except ValidationError as e:
            self.assertIn('personal_number', e.message_dict)
        else:
            raise Exception

    def test_invalid_email(self):
        book = Phonebook.objects.last()
        book.email = "invalid_email"
        try:
            book.full_clean()
        except ValidationError as e:
            self.assertIn('email', e.message_dict)
        else:
            raise Exception

    def test_invalid_landline_phone(self):
        book = Phonebook.objects.last()
        book.landline_phone = '606123456'
        try:
            book.full_clean()
        except ValidationError as e:
            self.assertIn('landline_phone', e.message_dict)
        else:
            raise Exception

    def test_invalid_cell_phone(self):
        book = Phonebook.objects.last()
        book.cell_phone = '+420606123456'
        try:
            book.full_clean()
        except ValidationError as e:
            self.assertIn('cell_phone', e.message_dict)
        else:
            raise Exception


class TestViews(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        department_test = Department.objects.create(
            name='test',
            abbreviation='TEST',
            code=10001,
        )
        department_obav = Department.objects.create(
            name='obav',
            abbreviation='OBAV',
            code=10002,
        )        
        Phonebook.objects.create(
            first_name='Milos',
            last_name='Riha',
            personal_number=10000,
            email='milos@sukl.cz',
            landline_phone='282185123',
            department=department_obav,
            building='24',
            room='666',
        )
        Phonebook.objects.create(
            first_name='Miloš',
            last_name='Říha',
            personal_number=12345,
            email='riha@sukl.cz',
            landline_phone='282185456',
            department=department_obav,
            building='24',
            room='666',
        )
        Phonebook.objects.create(
            first_name='Aleš',
            last_name='Jiný',
            personal_number=67890,
            email='jiny@sukl.cz',
            landline_phone='282185789',
            cell_phone='606732859',
            department=department_test,
            building='30',
            room='123',
        )

    def test_phonebook_view(self):
        response = self.client.get('/phonebook/')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, 'phonebook.html')
        self.assertContains(response, 'milos@sukl.cz')

    def test_phonebook_filtering_name_accented(self):
        response = self.client.get('/phonebook/?name=Říha&submit=Vyhledat')
        self.assertContains(response, 'milos@sukl.cz')
        self.assertContains(response, 'riha@sukl.cz')
        self.assertNotContains(response, 'jiny@sukl.cz')

    def test_phonebook_filtering_name_unaccented(self):
        response = self.client.get('/phonebook/?name=riha&submit=Vyhledat')
        self.assertContains(response, 'milos@sukl.cz')
        self.assertContains(response, 'riha@sukl.cz')
        self.assertNotContains(response, 'jiny@sukl.cz')

    def test_phonebook_filtering_personal_number(self):
        response = self.client.get('/phonebook/?personal_number=67890&submit=Vyhledat')
        self.assertContains(response, 'jiny@sukl.cz')
        self.assertNotContains(response, 'milos@sukl.cz')
    
    def test_phonebook_filtering_landline_phone(self):
        response = self.client.get('/phonebook/?phone=282185789&submit=Vyhledat')
        self.assertContains(response, 'jiny@sukl.cz')
        self.assertNotContains(response, 'milos@sukl.cz')

    def test_phonebook_filtering_cell_phone(self):
        response = self.client.get('/phonebook/?phone=606732859&submit=Vyhledat')
        self.assertContains(response, 'jiny@sukl.cz')
        self.assertNotContains(response, 'milos@sukl.cz')

    def test_phonebook_filtering_room(self):
        response = self.client.get('/phonebook/?room=123&submit=Vyhledat')
        self.assertContains(response, 'jiny@sukl.cz')
        self.assertNotContains(response, 'milos@sukl.cz')

    def test_phonebook_filtering_department(self):
        department_test = Department.objects.get(name='test')
        response = self.client.get(f'/phonebook/?department={department_test.id}&submit=Vyhledat')
        self.assertContains(response, 'jiny@sukl.cz')
        self.assertNotContains(response, 'milos@sukl.cz')